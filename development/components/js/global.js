// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddLogicCarousel from './modules/logicCarousel';
import AddLogicFullImage from './modules/logicFullImage';
import AddPlayYoutube from './modules/playYoutube';
import AddImageZoomer from './modules/imageZoomer';
import AddTabSwitcher from './modules/tabSwitcher';
import AddFetchServerData from './modules/fetchServerData';

// import AddFetchCarouselItems from './modules/fetchCarouselItems';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	new AddLogicCarousel('.js__logicCarousel', { sm: 1, md: 1, lg: 1, xl: 1, xxl: 1 }, 12, false).run();
	new AddLogicCarousel('.js__catalogCarousel', { sm: 1, md: 1, lg: 2, xl: 5, xxl: 6 }, 0, true).run();
	new AddLogicCarousel('.js__historyCarousel', { sm: 1, md: 1, lg: 2, xl: 5, xxl: 6 }, 0, true).run();

	new AddLogicFullImage('.js__logicCarousel', '.js__imageTakeit').run();
	new AddImageZoomer('.js__imageTakeit', '.js__imageResult').run();
	new AddTabSwitcher('.js__tabsAboutIn', '.js__tabsAboutOut').run();
	new AddPlayYoutube('.js__playYoutube').run();
	new AddMoveScrollUP('.js__moveScrollUP').run();

	new AddFetchServerData('.js__galleryData', {
		server: 'my-server.json',
		selector: 'data-gallery-init'
	}).run();

	// new AddFetchCarouselItems('.js__catalogCarousel', {
	// 	server: 'carousel-items.json',
	// 	selector: 'data-gallery-init'
	// }).run();

	// new AddtabSwitcher('.js__tabsAboutIn', '.js__tabsAboutOut').run();
};
if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
