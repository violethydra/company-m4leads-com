module.exports = class AddImageZoomer {
	constructor(enter, exit) {
		this.enter = enter;
		this.exit = exit;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.enter}`);
		const result = document.querySelector(`${this.exit}`);
		if (elem) {
			this.constructor.info();

			const imageZoom = () => {
				const lens = document.createElement('DIV');

				if (!elem.parentNode.firstElementChild.classList.contains('gallery__lenz')) {
					lens.setAttribute('class', 'gallery__lenz');
					elem.parentElement.insertBefore(lens, elem);
					result.style.visibility = 'visible';
				}

				const backgroundPos = () => {
					const cx = result.offsetWidth / lens.offsetWidth;
					const cy = result.offsetHeight / lens.offsetHeight;
					result.style.backgroundImage = `${elem.style.backgroundImage}`;
					result.style.backgroundSize = `${(elem.offsetWidth * cx)}px ${(elem.offsetHeight * cy)}px`;
				};

				const getCursorPos = (event) => {
					const a = elem.parentElement.getBoundingClientRect();
					let x = 0;
					let y = 0;

					x = event.pageX - a.left;
					y = event.pageY - a.top;

					x -= window.pageXOffset;
					y -= window.pageYOffset;

					return { x, y };
				};

				const moveLens = (event) => {
					event.preventDefault();

					const pos = getCursorPos(event);
					let x = pos.x - (lens.offsetWidth / 2);
					let y = pos.y - (lens.offsetHeight / 2);

					if (x > elem.offsetWidth - lens.offsetWidth) { x = elem.offsetWidth - lens.offsetWidth; }
					if (x < 0) { x = 0; }
					if (y > elem.offsetHeight - lens.offsetHeight) { y = elem.offsetHeight - lens.offsetHeight; }
					if (y < 0) { y = 0; }

					lens.style.left = `${x}px`;
					lens.style.top = `${y}px`;

					result.style.backgroundPosition = `-${(x * result.offsetWidth / lens.offsetWidth)}px -${(y * result.offsetHeight / lens.offsetHeight)}px`;
				};

				backgroundPos();
				lens.addEventListener('mousemove', moveLens);
				elem.addEventListener('mousemove', moveLens);
				lens.addEventListener('touchmove', moveLens);
				elem.addEventListener('touchmove', moveLens);
			};

			const imageZoomKill = (event) => {
				if (elem.parentNode.firstElementChild.classList.contains('gallery__lenz')) {
					event.target.firstElementChild.remove();
					result.style.visibility = 'hidden';
				}
			};

			elem.addEventListener('mouseenter', event => imageZoom(event));
			elem.parentElement.addEventListener('mouseleave', event => imageZoomKill(event));
		}
	}
};
